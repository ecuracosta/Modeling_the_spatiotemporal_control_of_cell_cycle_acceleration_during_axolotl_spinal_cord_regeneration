# Cura Costa et al., 2021a
Contains source code for simulations and data analysis performed for Cura Costa et al., 2021.
You can view the notebooks online using [Binder](http://mybinder.org/).
However, you should note that some calculations will take too much time to run them online. 

If you want to run the notebooks locally please note they use Python 3.9. 
The python environment that was used is contained in `requirements.txt`. 
